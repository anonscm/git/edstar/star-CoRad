/* Copyright (C) 2015-2018, 2021 |Meso|Star> (contact@meso-star.com)
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */

#include <rsys/float3.h>
#include <star/s3d.h>
#include <star/ssp.h>
#include <star/smc.h>
#include <math.h>
#include "scorad_realization.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*******************************************************************************
 * Helper function
 ******************************************************************************/

int copy_file(char const * const source, char const * const destination)
{
  FILE* fSrc;
  FILE* fDest;
  char buffer[512];
  int NbLus;
  if ((fSrc = fopen(source, "rb")) == NULL)
    {
      return 1;
    }
  if ((fDest = fopen(destination, "wb")) == NULL)
    {
      fclose(fSrc);
      return 2;
    }
  while ((NbLus = fread(buffer, 1, 512, fSrc)) != 0)
    fwrite(buffer, 1, NbLus, fDest);
  fclose(fDest);
  fclose(fSrc);

  return 0;
}

/*******************************************************************************
 * 4V/S integrand
 ******************************************************************************/
res_T
scorad_realization
  (void* out_length, 
  struct ssp_rng* rng, 
  const unsigned ithread, 
  void* context)
{
  struct smc_doubleN_context* dblN_ctx = context;
  const struct scorad_context* ctx = dblN_ctx->integrand_data;
/*  struct s3d_attrib attrib;
  struct s3d_primitive prim;*/

  double* result = SMC_DOUBLEN(out_length);
  /*double w = 0; Monte Carlo weight*/
  double extinction_length = 0.; /*Extinction length (m)*/
  double current_position, start_position, end_position = 0.; /*Positions during radiative walk (m)*/
  int EXIT_RADIATIVE_WALK = 0; /*Flag*/
  /*float r_phi, r_theta; Random variables for polar and azimutal angles*/
  /*double phi, theta; Azimutal and polar angles*/
  double direction; /*misc directions*/
  int start_index, end_index = 0, index = 0; /*index regarding position profile*/
  (void)ithread; /* Avoid "unused variable" warning */
  int i, count = 0, scattering = 1;
  double r_wall, direction_wall; /*Random variable and wall direction when reflection*/
  double r_path;
  double sample[3];
  double normal_left[3], normal_right[3];
  float u[3];
  double dir_test;
  normal_left[0] = 1.;
  normal_left[1] = 0.;
  normal_left[2] = 0.;

  normal_right[0] = -1.;
  normal_right[1] = 0.;
  normal_right[2] = 0.;
  FOR_EACH(i, 0, dblN_ctx->count){
    result[i] = 0.;
  }
  /*Sample a position between node + delta*/
  start_position = ssp_rng_canonical_float(rng) * ctx->thickness_slab;
  /*Set index_star in consequence*/
  start_index = 1 + (int)((start_position * (ctx->number_nodes - 1)) / ctx->thickness_slab);
  /*Initialize current position before starting radiative walk*/
  current_position = start_position; 
  /*
  ========START RADIATIVE WALK!=========
  */
  EXIT_RADIATIVE_WALK = 0;
  do{
    /*________________________________________________
    Quit straight away if start_position at a boundary
    ‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾*/
      /*__________________________________________
      Sample a unit direction & extinction length
      ‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾*/
      /*Sample an extinction length*/
      extinction_length = ssp_ran_exp(rng, ctx->extinction_coefficient);

      if(scattering == 1){
      /*Sample an unit direction*/
      f3_set_d3(u, ssp_ran_sphere_uniform(rng, sample, NULL));
      direction = u[0] * normal_left[0] + u[1] * normal_left[1] + u[2] * normal_left[2];
      /*_______________________
      Handle boundaries & move
      ‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾*/
      current_position = current_position + (extinction_length * direction);
      }
      count +=1;
      /*________________________________________
      Identify in which mesh is now end_positistart_on
      ‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾*/
      /*ONLY in absorption case --> path stops!*/
      if(current_position < 0.0) /*Reached the left boundary*/
      {
        r_wall = ssp_rng_canonical(rng); /*uniform random variable*/
        if(r_wall < ctx->emissivity) /*Absorption on the left wall*/
        {
          end_index = 0;
          end_position = current_position;
          EXIT_RADIATIVE_WALK = 1; /*===END OF RADIATIVE WALK===*/
        }
        else /*Reflection*/
        {
          ssp_ran_hemisphere_uniform(rng, normal_left, sample, NULL);
          f3_set_d3(u, sample);
          direction_wall = u[0] * normal_left[0] + u[1] * normal_left[1] + u[2] * normal_left[2]; /*Direction computation*/


          
              /*printf("left  %f  --  direction_wall  %f   -- %f  \n", current_position, direction_wall, direction);*/
          current_position = compute_reflected_position(0. , current_position, 
                                                        direction_wall, direction); 
          if(current_position > ctx->thickness_slab){
            scattering = 0;
          }
          else{
            scattering = 1;
          } 
          if(current_position < 0)
              printf("right reflected  %f  \n", current_position);
          direction = direction_wall;
          r_path = ssp_rng_canonical_float(rng);
          if(r_path > ctx->scattering_coefficient/ctx->extinction_coefficient){
            end_position = current_position;
            if(end_position < 0.0){
              end_index = 0;
            }
            else if(end_position > ctx->thickness_slab){
              end_index = ctx->number_nodes;
            }
            else{
              end_index = 1 + (int)((end_position * (ctx->number_nodes - 1)) / ctx->thickness_slab);
            }
            EXIT_RADIATIVE_WALK = 1; /*===END OF RADIATIVE WALK===*/
          }
          /*Else : Scattering!*/
         }
      }
      else if(current_position > ctx->thickness_slab)
      {
        r_wall = ssp_rng_canonical(rng); /*uniform random variable*/
        if(r_wall < ctx->emissivity){ /*Absorption on the right wall*/
          end_index = ctx->number_nodes; 
          end_position = current_position;
          EXIT_RADIATIVE_WALK = 1; /*===END OF RADIATIVE WALK===*/
        }
        else{ /*Reflection*/
          ssp_ran_hemisphere_uniform(rng, normal_right, sample, NULL);
          f3_set_d3(u, sample);
          direction_wall = u[0] * normal_left[0] + u[1] * normal_left[1] + u[2] * normal_left[2]; /*Direction computation*/
          if(direction_wall > 0.)
              printf("Error rignt  \n");
          current_position = compute_reflected_position(ctx->thickness_slab, current_position, 
                                                        direction_wall, direction);
          if(current_position < 0){
            scattering = 0;
          }
          else{
            scattering = 1;
          } 

          if(current_position > ctx->thickness_slab)
              printf("right reflected  %f  \n", current_position);
          direction = direction_wall;
          r_path = ssp_rng_canonical_float(rng);
          if(r_path > ctx->scattering_coefficient/ctx->extinction_coefficient){ /*Absorption!*/
            end_position = current_position;
            if(end_position < 0.0){
              end_index = 0;
            }
            else if(end_position > ctx->thickness_slab){
              end_index = ctx->number_nodes; 
            }
            else{
              end_index = 1 + (int)((end_position * (ctx->number_nodes - 1)) / ctx->thickness_slab);
            }
            EXIT_RADIATIVE_WALK = 1; /*===END OF RADIATIVE WALK===*/
          }
          /*Else : Scattering!*/
        } 
      }
      else{ /*Still in the volume*/
        r_path = ssp_rng_canonical_float(rng);
        if(r_path > (ctx->scattering_coefficient / ctx->extinction_coefficient)){ /*Absorption!*/
          end_position = current_position;
          end_index = 1 + (int)((end_position * (ctx->number_nodes - 1)) / ctx->thickness_slab);
          EXIT_RADIATIVE_WALK = 1; /*===END OF RADIATIVE WALK===*/
        }
        /*Else : Scattering!*/
        else{
          scattering = 1;
        }
/*    if(EXIT_RADIATIVE_WALK == 0)
        printf(" %d\n", EXIT_RADIATIVE_WALK);*/
    }
  }while(EXIT_RADIATIVE_WALK == 0);
  index = ((start_index + 1 ) * (ctx->number_nodes + 1)) + end_index ;
  result[start_index] = 1.;
  result[index] = 1.;

  return RES_OK;
}

double compute_reflected_position(double thickness_slab, double current_position, 
                                  double direction_wall, double direction){
  double length = 0.;
  double mem = current_position;
  length = thickness_slab + (direction_wall * ((current_position - thickness_slab) / direction));
/*if(length < 0.)
              printf("left   %f --   %f  \n", length, current_position);
if(length > 1.)
              printf("right  %f --   %f \n", length, current_position);      */
  return length;
}

extern void initialize_temperature_profile(struct scorad_context* ctx, double* temperature_profile, 
                                   double temp_left, double temp_right){
  double delta = ctx->thickness_slab / (ctx->number_nodes - 1);
  double a = (temp_right - temp_left) / ctx->thickness_slab;
  int i = 0;
  temperature_profile[i] = temp_left;
  temperature_profile[ctx->number_nodes - 1] = temp_right;
  FOR_EACH(i, 1, ctx->number_nodes){
    temperature_profile[i] = a * i * delta + temp_left;
  }
}


void empty_buffer(){
  int c = 0;
  while (c != '\n' && c != EOF){
    c = getchar();
  }
}
 
int read_char(char *chaine, int longueur){
  char *positionEntree = NULL;
  if (fgets(chaine, longueur, stdin) != NULL) {
    positionEntree = strchr(chaine, '\n');
    if (positionEntree != NULL){
      *positionEntree = '\0';
    }
    else{
      empty_buffer();
    }
  return 1;
  }
  else{
    empty_buffer();
    return 0;
  }
}

const char* get_temperature_settings(int case_ref, int temp){
  char case_reference[20];
  char temp_choice[3];
  char *copie = NULL;
  
  sprintf(case_reference, "case%d", case_ref);

  if(temp == 0){
    strncat(case_reference,".yaml",30);
  }
  else{
    int out = 0;
    while(out == 0){
      printf(" Temperature settings:\n\
 A - right temperature = 2 x left temperature \n\
 B - right temperature = 10 x left temperature \n");
      read_char(temp_choice,3);
      if (strcmp(temp_choice,"A") == 0){
        strncat(case_reference,"_2T.yaml",30);
        out = 1;
      }
      else if (strcmp(temp_choice,"B") == 0){  
        strncat(case_reference,"_10T.yaml",30);
        out = 1;
      }
      else {
        printf(" Bad choice, same player still plays !!!\n\n");
      }
    }
  }
  copie=malloc((strlen(case_reference)+1)*sizeof(char));  
  strcpy(copie, case_reference);
  return copie;
}
