/* Copyright (C) 2015-2018, 2021 |Meso|Star> (contact@meso-star.com)
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */

#ifndef SCORAD_REALIZATION_H
#define SCORAD_REALIZATION_H

#include <rsys/rsys.h>

#define SB 5.67E-8
#define M_PI 3.14159265358979323846
/*#define NUMBER_NODES 21*/ /*Number of nodes at which temperature is evaluated*/

/* forward definition */
struct ssp_rng;

struct temp;

/*Collection of informations regarding the context of the studied problem*/
struct scorad_context {
  double thickness_slab; /*Thickness of the slab (m)*/
  double stark_number; /*N = ka * lambda/(4*sigma_SB*T_ref^3) (-)*/
  double tau; /*Optical thickness of the slab (-) tau = ka*L*/
  double extinction_coefficient; /*Extinction coefficient (1/m)*/
  double absorption_coefficient; /*Absorption coefficient (1/m) */
  double scattering_coefficient; /*Scattering coefficient (1/m) */
  double emissivity; /*Walls emissivities (-)*/
  double right_temperature; /*Temperature on the right side of the slab (K)*/
  double left_temperature; /*Temperature on the left side of the slab (K)*/
  double lambda; /*Thermal conductivity of the medium (W.m^-1.K^-1)*/  
  double spatial_step; /*Spatial step for temperature/position profile discretisation (m)*/
  double position_start;
  double number_mc_realizations;

  int number_nodes; /*Number of nodes at which temperature is evaluated*/
  int node;   /*index of current node */
  int number_maximum_iterations_finite_differences; /*Number of maximum iterations in the finite difference loop*/
};

/* Hit filter function used to handle auto intersection */

int copy_file(char const * const source, char const * const destination);
extern void get_yaml_values(double *values, char* s);

/*******************************************************************************
 * MC realization function
 ******************************************************************************/
extern res_T
scorad_realization
  (void* length,
   struct ssp_rng* rng,
   const unsigned ithread,
   void* context);

double compute_reflected_position(double thickness_slab, double current_position, 
                                  double direction_wall, double direction);

const char* get_temperature_settings(int case_ref, int temp);

extern void initialize_temperature_profile(struct scorad_context* ctx, double* temperature_profile, 
            double temp_left, double temp_right);

void empty_buffer();

int read_char(char *chaine, int longueur);

#endif /* REALIZATION_H */
