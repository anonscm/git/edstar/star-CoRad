/*
C code in which a list of temperature at a given position along the thickness of a medium are computed. 
The Monte Carlo algorithm solves the 1D steady state heat equation with a volumetric source term
corresponding to the **non-linearized** Radiative Transfer Equation in Intensity. Radiation
occurs through emission/absorption and **scattering** events. The boundary conditions are Dirichlet 
ones, temperatures are imposed both on the left and right sides of the medium. 
Boundaries of the plate can be black or grey opaque surfaces (emissivity = 1 or != 1)

_Note_ : The temperature profile and position dicretizement is not depending on the spatial step used 
for the Monte Carlo computation. The latter can be adapted according to a compromise between numerical 
precision and computation time.

              |                                       |
    T_left    |-x---x---x---x---x--...---x---x---x--x-| T_right
              |                                       |
*/

#include <rsys/cstr.h>
#include <rsys/float3.h>
#include <rsys/mem_allocator.h>
#include <rsys/clock_time.h>
#include <star/s3d.h>
#include <star/s3daw.h>
#include <star/smc.h>
#include <yaml.h>
#include "scorad_realization.h"
#include <stdlib.h>
#include <stdio.h>
#include <sys/stat.h>
#include <time.h>


#define RESULTS_PATH "results"

extern void get_yaml_values(double *values, char* s){
  FILE *fh = fopen(s, "r");
  yaml_parser_t parser;
  yaml_document_t doc;
  yaml_node_t *node;
  int a;
  yaml_node_t* key;
  yaml_node_t* val;
  intptr_t n;
  double dst;
  assert(fh);
  assert(yaml_parser_initialize(&parser));
  yaml_parser_set_input_file(&parser, fh);
  if (!yaml_parser_load(&parser, &doc)) {
    goto done;
  }

  for( a = 1; a < 10; a++ ){
    node = yaml_document_get_node(&doc, a);
    if(!node) break;
    if(node->type == YAML_MAPPING_NODE) {
      n = node->data.mapping.pairs.top - node->data.mapping.pairs.start;
      for (a = 0; a < n; a++ ){
        key = yaml_document_get_node(&doc, node->data.mapping.pairs.start[a].key);
        val = yaml_document_get_node(&doc, node->data.mapping.pairs.start[a].value);
        cstr_to_double((char*)val->data.scalar.value, &dst);
        values[a] = dst;
      }
    }
  }
  yaml_document_delete(&doc);

  done:
    yaml_parser_delete(&parser);
    assert(!fclose(fh));
}

static res_T
compute_scorad
  (/*struct s3d_scene* scene,*/
  /*const size_t max_realisations,*/
  double *values,
   const char* smc_filename,
   const char* backup_file)
{
  /*Declaration of structures*/
  struct scorad_context ctx;
  struct smc_doubleN_context dblN_ctx;
  struct smc_device* smc = NULL;
  struct smc_integrator integrator;
  struct smc_estimator* estimator = NULL;
  struct smc_estimator_status estimator_status;

  const size_t max_realisations = values[7]; 
  double left_temperature, right_temperature, hot_temperature;
  int i, j, k, result_repo, copy;  
  char path[255] = "./results/";
  char settings[255]= "./results/";
  char path_res[255] = "./results/";
  char format[128];
  time_t temps;
  struct tm date;
  FILE* fichier = NULL;
  double* temp_old = NULL;
  double* temp_new = NULL;
  double* div_q = NULL;
  double* source_term = NULL;
  double* smc_coeff = NULL;
  double incident, delta; 
  double criterion = 1., tolerance = values[8];
  int ind = 0, ind_bis = 0, nb_smc;

  res_T res = RES_OK;

  if(values[5] > values[6])
  {
    hot_temperature = values[5];
  }
  else
  {
    hot_temperature = values[6];
  }
  ctx.number_maximum_iterations_finite_differences = values[0]; /*atoi(argv[4]); /*5*pow(10,-4);*/
  ctx.number_nodes = values[0];  /*atoi(argv[3]);*/
  ctx.thickness_slab = values[1]; /*Thickness of the slab (m)*/ 
  ctx.stark_number = values[2]; /*N = ka * lambda/(4*sigma_SB*T_ref^3) (-)*/
  ctx.emissivity = values[3];
  ctx.extinction_coefficient = 1/values[1];
  ctx.scattering_coefficient = values[4];
  ctx.absorption_coefficient = ctx.extinction_coefficient - ctx.scattering_coefficient; 
  ctx.tau = ctx.extinction_coefficient * ctx.thickness_slab; /*Optical thickness of the slab (-) tau = ka*L*/
  ctx.left_temperature = values[5]; /*Temperature on the left side of the slab (K)*/
  ctx.right_temperature = values[6]; /*Temperature on the right side of the slab (K)*/
  /*Thermal conductivity of the medium (W.m^-1.K^-1)*/  
  ctx.lambda = (4 * SB * hot_temperature * hot_temperature * hot_temperature * ctx.stark_number) 
                / ctx.extinction_coefficient ;
  ctx.spatial_step = ctx.thickness_slab/((double)ctx.number_nodes - 1); /*Spatial step for discretization (m)*/
  ctx.number_mc_realizations = values[7];

  dblN_ctx.count = (size_t)((ctx.number_nodes + 1) * (ctx.number_nodes + 1) + (ctx.number_nodes + 1));
  dblN_ctx.integrand_data = &ctx;

  left_temperature = values[5];
  right_temperature = values[6];
  delta = ctx.thickness_slab / (ctx.number_nodes - 1);  


  if (strcmp(backup_file,"") != 0){  
    strcpy(format,backup_file);
  }
  else{
    time(&temps);
    date=*localtime(&temps);
    strftime(format, 128, "%y%m%d_%H%M", &date);
  }

  strcat(path, format);
  strcat(path_res, format);
  strcat(settings, format);
  strcat(settings, "/settings.yaml");
  result_repo = mkdir(RESULTS_PATH, 0777);
  result_repo = mkdir(path, 0777);
  copy = copy_file("settings.yaml", settings);
  

  nb_smc = ((ctx.number_nodes - 1) * (ctx.number_nodes + 1));


  if (strcmp(smc_filename,"results_smc.dat") == 0){
    /*Checking realisations are OK*/
    ASSERT(max_realisations > 0);
    /* Setting up parameters for MC computation*/
    SMC(device_create(NULL, NULL, SMC_NTHREADS_DEFAULT, SSP_RNG_TYPE_NULL, &smc));
    integrator.integrand = scorad_realization; /* Realization function */
    integrator.type = &smc_doubleN; /* Type of the Monte Carlo weight */
    integrator.max_realisations = max_realisations; /* Realization count */
    integrator.max_failures = max_realisations / 1000;
    
    /*Solving with MC & getting results display*/
    SMC(solve(smc, &integrator, &dblN_ctx, &estimator)); /*/!\ ctx passé en argument pointeur!*/
    SMC(estimator_get_status(estimator, &estimator_status));

    /* Print the simulation results -. NOT NEEDED ?? */
    SMC(estimator_get_status(estimator, &estimator_status));
    double convergence = 0.;
    smc_coeff = mem_calloc((size_t)((ctx.number_nodes - 1) * (ctx.number_nodes + 1)), sizeof(double));
    FOR_EACH(j, 2, ctx.number_nodes + 1){
      FOR_EACH(k, 0, ctx.number_nodes + 1){
        ind =  (int)(((j) * (ctx.number_nodes + 1)) + k);
        ind_bis = (int)((j - 2) * (ctx.number_nodes + 1) + k);
        smc_coeff[ind_bis]= (SMC_DOUBLEN(estimator_status.E)[ind] 
                           / SMC_DOUBLEN(estimator_status.E)[j-1]) ;
        convergence = 100 * SMC_DOUBLEN(estimator_status.SE)[ind] / SMC_DOUBLEN(estimator_status.E)[ind];
        if(convergence > 5.1){
          printf("  convergence  %f  \n",convergence);
        }
        
/*        printf("%f \n", smc_coeff[ind_bis]);*/
      }
    }

    strcat(path, "/results_smc.dat");
    fichier = fopen(path, "w+");
    if (fichier != NULL){
      FOR_EACH(i, 0, (int)nb_smc){
        fprintf(fichier,"%.6f \n", smc_coeff[i]);
      }
      fclose(fichier);
    } 
    else {
      printf("Unable to open the file\n");
    }
  }
  else {
    fichier = fopen(smc_filename,"r");
    if (fichier != NULL) {
      struct stat sb;
      stat(smc_filename, &sb);
      char *file_contents = malloc(sb.st_size); 
      smc_coeff = mem_calloc((size_t)(sb.st_size), sizeof(double));
      i = 0;
      while (fscanf(fichier, "%[^\n] ", file_contents) != EOF) {
        sscanf(file_contents, "%lf", &smc_coeff[i]);
        i += 1;
      } 
      fclose(fichier);
    }  
  }


  /* Finite difference  */
  
  temp_old = mem_calloc((size_t)(ctx.number_nodes), sizeof(double)); 
  temp_new = mem_calloc((size_t)(ctx.number_nodes), sizeof(double)); 
  div_q = mem_calloc((size_t)(ctx.number_nodes), sizeof(double)); 
  source_term = mem_calloc((size_t)(ctx.number_nodes), sizeof(double)); 

  initialize_temperature_profile(&ctx, temp_old, left_temperature, right_temperature);
  temp_new[0] = left_temperature;
  temp_new[ctx.number_nodes - 1] = right_temperature;



  while(criterion > tolerance){
    FOR_EACH(j, 0, ctx.number_nodes - 1){
      incident = 0. ;
      FOR_EACH(k, 0, ctx.number_nodes + 1){
        ind = (int)(j * (ctx.number_nodes + 1) + k);
        if(k == 0){
          incident += smc_coeff[ind]
              * pow(left_temperature,4.);
        } else if(k == (ctx.number_nodes)){
          incident += smc_coeff[ind]
              * pow(right_temperature,4.);
        }
        else{
          incident += smc_coeff[ind]
              * pow((temp_old[k - 1] + temp_old[k]) / 2.,4.);
        }
      }
      div_q[j] = delta * ctx.absorption_coefficient * 4 
                 * SB * (pow((temp_old[j] + temp_old[j + 1]) / 2.,4.) - incident);
    }
    /* source term  */
    FOR_EACH(j, 0, ctx.number_nodes - 1){
      source_term[j] = (div_q[j] + div_q[j + 1]) / (2. * delta);
    }
    FOR_EACH(j, 1, ctx.number_nodes - 1){
    }
    /* temp  */
    FOR_EACH(j, 1, ctx.number_nodes - 1){
      temp_new[j] = ((temp_old[j + 1] + temp_old[j - 1]) / 2.) 
                    - ((delta * delta * source_term[j-1]) / (2. * ctx.lambda));
    }
    criterion = 0.;
    FOR_EACH(j, 1, ctx.number_nodes - 1){
      criterion += fabs((temp_old[j] - temp_new[j])/temp_new[j]) ;
    }
    FOR_EACH(j, 1, ctx.number_nodes - 1){
      temp_old[j] = temp_new[j];
    }
  }
  strcat(path_res, "/results.dat");
  fichier = fopen(path_res, "w+");
  if (fichier != NULL){
    FOR_EACH(i, 0, (int)ctx.number_nodes){
      fprintf(fichier,"%.6f \n", temp_new[i]);
    }
    fclose(fichier);
  } else {
    printf("Unable to open the file");
  }
  
  exit:
  /* Clean-up data */
    /*if(view) S3D(scene_view_ref_put(view));*/
    if(smc) SMC(device_ref_put(smc));
    if(estimator) SMC(estimator_ref_put(estimator));
    mem_rm(temp_old);
    mem_rm(temp_new);
    mem_rm(div_q);
    mem_rm(source_term);
    mem_rm(smc_coeff);

    return res;

  error:
    goto exit;
}

/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/

int
main(int argc, char* argv[])
{
  /*******************************************************************************
  * Declarations
  ******************************************************************************/
  /*Declarations of structures to use*/
  struct s3d_scene* scene = NULL; /*Collection of shapes*/
  struct time t0, t1;
  /*Declaration of miscellaneous variables*/
  char dump[64];
  /*unsigned long nrealisations = 10000; Default number of realizations for MC calculation*/
  res_T res = RES_OK;
  struct ssp_rng* rng;
  int err = 0;
  char settings_filename[30];
  char *settings_out[30];
  char smc_choice[3];
  char smc_filename[30];
  char backup_file[30];
  int smc_file = 0;
  double settings[9] = {0.};
  FILE *file;

  if(argc < 1 || argc > 1) {
    printf("Usage: %s \n", argv[0]);
    goto error;
  }

  char comp_file[] = "case1_2T.yaml";
  /*******************************************************************************
  * Assignments & Checkings
  ******************************************************************************/
  
  time_current(&t0);
  time_sub(&t0, time_current(&t1), &t0);
  time_dump(&t0, TIME_ALL, NULL, dump, sizeof(dump));
  int choice = 0;
  while(choice == 0){
    printf(" Fonctionnal Monte Carlo Coefficients:\n\
 0 - Compute new coefficients according to settings.yaml \n\
 1 - Case 1: black walls, pure absorbing medium, Stark number = 10, 40 nodes \n\
 2 - Case 2: black walls, pure absorbing medium, Stark number = 1, 40 nodes \n\
 3 - Case 3: black walls, pure absorbing medium, Stark number = 0.1, 40 nodes \n\
 4 - Case 4: black walls, pure absorbing medium, Stark number = 0.01, 40 nodes \n\
 5 - Case 5: grey walls, pure absorbing medium, Stark number = 0.01, eps = 0.5, 40 nodes \n\
 6 - Case 6: grey walls, pure absorbing medium, Stark number = 0.01, eps = 0.1, 40 nodes \n\
 7 - Case 7: black walls, absorbing scattering medium, Stark number = 0.1, ks = 0.5, 40 nodes \n\
 8 - Case 8: black walls, absorbing scattering medium, Stark number = 0.1, ks = 1., 40 nodes \n\
 9 - User case : set a user specific file name in line with settings.yaml \n");

    read_char(smc_choice,3);

  if (strcmp(smc_choice,"0") == 0){  
    strcpy(smc_filename,"results_smc.dat");
    strcpy(settings_filename,"settings.yaml");
    choice = 1;
  }
  else if (strcmp(smc_choice,"1") == 0){  
    strcpy(smc_filename,"case1.dat");
    strcpy(settings_filename, get_temperature_settings(1, 1));
    choice = 1;
  }
  else if (strcmp(smc_choice,"2") == 0){  
    strcpy(smc_filename,"case2.dat");
    strcpy(settings_filename, get_temperature_settings(2, 1));
    choice = 1;
  }
  else if (strcmp(smc_choice,"3") == 0){  
    strcpy(smc_filename,"case3.dat");
    strcpy(settings_filename, get_temperature_settings(3, 1));
    choice = 1;
  }
  else if (strcmp(smc_choice,"4") == 0){  
    strcpy(smc_filename,"case4.dat");
    strcpy(settings_filename, get_temperature_settings(4, 1));
    choice = 1;
  }
  else if (strcmp(smc_choice,"5") == 0){  
    strcpy(smc_filename,"case5.dat");
    strcpy(settings_filename, get_temperature_settings(5, 0));
    choice = 1;
  }
  else if (strcmp(smc_choice,"6") == 0){  
    strcpy(smc_filename,"case6.dat");
    strcpy(settings_filename, get_temperature_settings(6, 0));
    choice = 1;
  }
  else if (strcmp(smc_choice,"7") == 0){  
    strcpy(smc_filename,"case7.dat");
    strcpy(settings_filename, get_temperature_settings(8, 1));
    choice = 1;
  }
  else if (strcmp(smc_choice,"8") == 0){  
    strcpy(smc_filename,"case8.dat");
    strcpy(settings_filename, get_temperature_settings(9, 1));
    choice = 1;
  }
  else if(strcmp(smc_choice,"9") == 0){
    strcpy(settings_filename,"settings.yaml");
    choice = 1;
    printf(" Set user specific file name\n");
    read_char(smc_filename,30);
    if (file = fopen(smc_filename, "r")){
        fclose(file);
    }
    else{
      strcpy(smc_filename,"results_smc.dat"); 
      printf(" File doesn't exist, new coefficients will be generated\n");
    }     
  }
  else{
    printf(" Bad choice, same player still plays !!!\n\n");
  }
}
  get_yaml_values(settings, settings_filename);

  printf("\n Set backup directory name\n\ If you leave blank a directory will \ 
be created according to the time and date\n");
  read_char(backup_file,30);
  printf("Starting computation\n");
  
  res = compute_scorad(settings, smc_filename, backup_file);

  if(res != RES_OK) {
    fprintf(stderr, "Error !!! \n");
    goto error;
  }

  /*Memory release*/
  
exit:
  if(scene) S3D(scene_ref_put(scene));
  if(mem_allocated_size() != 0) 
  {
    fprintf(stderr, "Memory leaks: %lu Bytes\n",
    (unsigned long)mem_allocated_size());
    err = 1;
  }
  return err;
  
  error:
    err = 1;
  goto exit;

}
/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/
