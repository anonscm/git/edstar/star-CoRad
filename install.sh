#!/bin/sh
#
p=$(pwd)

git checkout 0.4.1
git clone https://gitlab.com/meso-star/star-engine.git
cd star-engine
git checkout 0.14.0
mkdir -p build
cd build
cmake ../cmake
make
cd ../../.

git clone https://github.com/yaml/libyaml.git
cd libyaml
git checkout 0.2.5
cmake -DCMAKE_INSTALL_PREFIX=../star-engine/local
make
make install
cd ..

mkdir -p build
mkdir -p etc

echo "#!/bin/sh" >> etc/star-CoRad.profile
echo "p=${pwd}" >> etc/star-CoRad.profile
echo "export LD_LIBRARY_PATH=$p/star-engine/local/lib:${LD_LIBRARY_PATH}" >> etc/star-CoRad.profile
echo "export PATH=$p/star-engine/local/bin:${PATH}" >> etc/star-CoRad.profile
echo "export MANPATH=$p/star-engine/local/share/man/:${MANPATH}" >> etc/star-CoRad.profile
echo "export CPATH=$p/star-engine/local/include:${CPATH}" >> etc/star-CoRad.profile
echo "export LIBRARY_PATH=$p/star-engine/local/lib:${LIBRARY_PATH}" >> etc/star-CoRad.profile
cd build
cmake -G "Unix Makefiles" -DCMAKE_PREFIX_PATH="$p/star-engine/local/" -DCMAKE_INSTALL_PREFIX="$p" ../cmake
make
make install
cd $p
source etc/star-CoRad.profile
cp data/* compute/.
