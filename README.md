# Star-CoRad

C code in which a list of temperature at a given position along the thickness of a medium are computed. 
The Monte Carlo algorithm solves the 1D steady state heat equation with a volumetric source term
corresponding to the **non-linearized** Radiative Transfer Equation in Intensity. Radiation
occurs through emission/absorption and **scattering**. The boundary conditions are Dirichlet 
ones, temperatures are imposed both on the left and right sides of the medium. 
Boundaries of the plate can be black or grey opaque surfaces (emissivity = 1 or != 1).

_Note_ : The temperature profile and position dicretizement are not depending on the spatial step used 
for the Monte Carlo computation. The latter can be adapted according to a compromise between numerical 
precision and computation time.

              |                                       |
    T_left    |-x---x---x---x---x--...---x---x---x--x-| T_right
              |                                       |

## How to build

Star-CoRad relies on [CMake](http://www.cmake.org) and the
[RCMake](https://gitlab.com/vaplv/rcmake/#tab-readme) package to build. It also
depends on the
[libYaml](https://github.com/yaml/libyaml.git) v0.2.5 and
[Star-engine](https://gitlab.com/meso-star/star-engine/) v0.14.0 libraries.

First ensure that CMake is installed on your system.
Then, clone the complete project on the sourcesup forge:

    ~ $ git clone https://git.renater.fr/anonscm/git/edstar/star-CoRad.git
    ~ $ cd star-Corad

The installation of star-CoRad code and its dependencies is automated
with the install.sh script. To install this code, run the following command:

    ~/ $ . ./install.sh

## How to use

To execute the code, you must first run the ./etc/star-CoRad.profile script to update the
paths to the various libraries

    ~/ $ . ./etc/star-CoRad.profile


Go to the compute directory

    ~/ $ cd compute

The directory contains the different files to execute the case studies as well as
the star-CoRad executable and the settings.yaml file. You must then execute the
code and follow the instructions.

    ~/ $ ./scorad

## License

Star-CoRad is free software released under the GPL v3+ license: GNU GPL version 3 or
later. You are welcome to redistribute it under certain conditions; refer to
the LICENSE.md file for details.
